package com.afs.restapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> findAll() {
        return this.employeeRepository.findAll();
    }

    public Employee findById(Long id) {
        return this.employeeRepository.findById(id);
    }

    public List<Employee> findByGender(String gender) {
        return this.employeeRepository.findByGender(gender);
    }

    public List<Employee> findByPage(Integer pageNumber, Integer pageSize) {
        return this.employeeRepository.findByPage(pageNumber, pageSize);
    }

    public Employee create(Employee newEmployee) {
        if (newEmployee.getAge() < 18 || newEmployee.getAge() > 65)
            throw new InvalidEmployeeException();
        return this.employeeRepository.insert(newEmployee);
    }

    public void delete(Long id) {
        this.employeeRepository.delete(id);
    }

    public Employee update(Long id, Employee employee) {
        Employee employeeToUpdate = findById(id);
        if (!employeeToUpdate.isActive())
            throw new InactiveEmployeeException();
        return this.employeeRepository.update(id, employee);
    }
}
