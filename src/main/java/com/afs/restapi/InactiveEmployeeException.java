package com.afs.restapi;

public class InactiveEmployeeException extends RuntimeException {
    public InactiveEmployeeException() {
        super("Employee is inactive.");
    }
}
