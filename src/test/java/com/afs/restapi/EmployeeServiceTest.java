package com.afs.restapi;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class EmployeeServiceTest {

    @Mock
    EmployeeRepository employeeRepository;

    @InjectMocks
    private EmployeeService employeeService;

    @Test
    void should_throw_invalid_employee_when_create_given_employee_age_less_than_18() {
        //given
        Employee newEmployee = new Employee("Tom", 16, "Male", 1000);

        //when
        //then
        String exceptionMessage = assertThrows(InvalidEmployeeException.class, () -> {
            employeeService.create(newEmployee);
        }).getMessage();
        assertEquals("Employee must be 18-65 years old.", exceptionMessage);
    }

    @Test
    void should_throw_invalid_employee_when_create_given_employee_age_greater_than_65() {
        //given
        Employee newEmployee = new Employee("Tom", 68, "Male", 1000);

        //when
        //then
        String exceptionMessage = assertThrows(InvalidEmployeeException.class, () -> {
            employeeService.create(newEmployee);
        }).getMessage();
        assertEquals("Employee must be 18-65 years old.", exceptionMessage);
    }

    @Test
    void should_add_new_employee_when_create_given_employee_age_in_range() {
        //given
        Employee newEmployee = new Employee("Tom", 45, "Male", 50000);

        //when
        employeeService.create(newEmployee);

        //then
        verify(employeeRepository, times(1)).insert(newEmployee);
    }

    @Test
    void should_delete_specific_employee_when_delete_given_employee_id() {
        //given

        //when
        employeeService.delete(1L);

        //then
        verify(employeeRepository, times(1)).delete(1L);
    }

    @Test
    void should_find_all_employees_when_find_all_given_employees() {
        //given

        //when
        employeeService.findAll();

        //then
        verify(employeeRepository, times(1)).findAll();
    }

    @Test
    void should_find_specific_employee_when_find_by_id_given_employee_id() {
        //given

        //when
        employeeService.findById(1L);

        //then
        verify(employeeRepository, times(1)).findById(1L);
    }

    @Test
    void should_find_male_employees_when_find_by_gender_given_employee_gender() {
        //given

        //when
        employeeService.findByGender("Male");

        //then
        verify(employeeRepository, times(1)).findByGender("Male");
    }

    @Test
    void should_return_correct_page_and_number_of_employees_when_find_by_page_given_page_number_and_size() {
        //given

        //when
        employeeService.findByPage(2, 3);

        //then
        verify(employeeRepository, times(1)).findByPage(2, 3);
    }

    @Test
    void should_update_employee_when_update_given_active_employee_with_age_and_salary() {
        //given
        Employee employeeToUpdate = new Employee(1L, "John Smith", 32, "Male", 5000);
        Employee updateInfo = new Employee("Tom", 48, "Male", 60000);
        when(employeeRepository.findById(1L)).thenReturn(employeeToUpdate);

        //when
        employeeService.update(1L, updateInfo);

        //then
        verify(employeeRepository, times(1)).update(1L, updateInfo);
    }

    @Test
    void should_throw_inactive_employee_exception_when_update_given_inactive_employee() {
        //given
        Employee employeeToUpdate = new Employee(1L, "John Smith", 32, "Male", 5000);
        Employee updateInfo = new Employee("John Smith", 48, "Male", 7000);
        when(employeeRepository.findById(1L)).thenReturn(employeeToUpdate);
        employeeRepository.findById(1L).setActive(false);

        //when
        //then
        String exceptionMessage = assertThrows(InactiveEmployeeException.class, () -> {
            employeeService.update(1L, updateInfo);
        }).getMessage();
        assertEquals("Employee is inactive.", exceptionMessage);
    }
}
