package com.afs.restapi;

import com.afs.restapi.exception.NotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {

    @Autowired
    MockMvc postmanMock;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    EmployeeRepository employeeRepository;

    @BeforeEach
    void setUp() {
        employeeRepository.reset();
    }

    @Test
    void should_return_all_employees_when_perform_get_given_employees() throws Exception {
        //given

        //when
        postmanMock.perform(MockMvcRequestBuilders.get("/employees"))
                //then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(5)));
    }

    @Test
    void should_return_specific_employee_when_perform_get_given_employee_id() throws Exception {
        //given
        String expect = objectMapper.writeValueAsString(new Employee(1L, "John Smith", 32, "Male", 5000));

        //when
        postmanMock.perform(MockMvcRequestBuilders.get("/employees/1"))
                //then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(result -> assertEquals(expect, result.getResponse().getContentAsString()));
    }

    @Test
    void should_return_404_when_perform_get_by_id_given_employee_not_exist() throws Exception {
        //given

        //when
        postmanMock.perform(MockMvcRequestBuilders.get("/employees/999"))
                //then
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void should_employee_status_be_false_when_perform_delete_given_employee_id() throws Exception {
        //given

        //when
        postmanMock.perform(MockMvcRequestBuilders.delete("/employees/1"))
                //then
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        assertFalse(employeeRepository.findById(1L).isActive());
    }

    @Test
    void should_return_male_employees_when_perform_get_given_employee_gender() throws Exception {
        //given

        //when
        postmanMock.perform(MockMvcRequestBuilders.get("/employees").param("gender", "Male"))
                //then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(3)))
                .andExpect(MockMvcResultMatchers.jsonPath("$..gender").value(everyItem(is("Male"))));
    }

    @Test
    void should_return_right_page_with_correct_number_of_employees_when_perform_get_given_page_number_and_size() throws Exception {
        //given
        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(new Employee(3L, "David Williams", 35,"Male", 5500));
        employeeList.add(new Employee(4L, "Emily Brown", 23, "Female", 4500));
        String expect = objectMapper.writeValueAsString(employeeList);

        //when
        postmanMock.perform(MockMvcRequestBuilders.get("/employees").param("pageNumber", "2").param("pageSize", "2"))
                //then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(result -> assertEquals(expect, result.getResponse().getContentAsString()));
    }

    @Test
    void should_return_new_employee_when_perform_post_given_new_employee() throws Exception {
        //given
        employeeRepository.clearAll();
        String newEmployeeJson = "{\n" +
                "\"name\": \"Lisa 10\",\n" +
                "\"age\": 22,\n" +
                "\"gender\": \"Female\",\n" +
                "\"salary\": 9000\n" +
                "}";

        //when
        postmanMock.perform(MockMvcRequestBuilders.post("/employees").contentType(MediaType.APPLICATION_JSON).content(newEmployeeJson))
                //then
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lisa 10"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty());

        List<Employee> all = employeeRepository.findAll();
        assertEquals(1, all.size());
        assertEquals("Lisa 10", all.get(0).getName());
        assertNotNull(all.get(0).getId());
    }

    @Test
    void should_return_updated_employee_when_perform_put_given_age_and_salary() throws Exception {
        //given
        String expect = objectMapper.writeValueAsString(new Employee(1L, "John Smith", 18, "Male", 60000));
        String newEmployeeJson = "{\n" +
                "\"age\": 18,\n" +
                "\"salary\": 60000\n" +
                "}";

        //when
        postmanMock.perform(MockMvcRequestBuilders.put("/employees/1").contentType(MediaType.APPLICATION_JSON).content(newEmployeeJson))
                //then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(result -> assertEquals(expect, result.getResponse().getContentAsString()));
    }

    @Test
    void should_return_404_when_perform_put_given_employee_not_exist() throws Exception {
        //given
        String newEmployeeJson = "{\n" +
                "\"age\": 18,\n" +
                "\"salary\": 60000\n" +
                "}";

        //when
        postmanMock.perform(MockMvcRequestBuilders.put("/employees/999").contentType(MediaType.APPLICATION_JSON).content(newEmployeeJson))
                //then
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void should_return_404_when_perform_delete_given_employee_not_exist() throws Exception {
        //given

        //when
        postmanMock.perform(MockMvcRequestBuilders.delete("/employees/999"))
                //then
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }
}
